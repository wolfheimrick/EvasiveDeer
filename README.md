# Unity3D Template - Comment bien débuter un projet

La chose la plus importante avant de commencer… est peut être déjà d'avoir un projet. Qu'il soit petit ou grand, avoir un objectif permet d'avancer par étape. Si l'objectif est trop grand, il est possible de le diviser en différentes parties plus abordable.

Unity3D est un moteur de jeux permettant une prise en main intuitive, tout comme Unreal Engine. Intuitive ne veut pas dire simple: ces deux moteurs sont adaptés à un large public incluant programmeuseurs, game designeuseurs, artistes,… D'autres moteurs peuvent être moins accessible, surtout en tant qu'indépendant·e·s ou débutant·e·s.

Nous allons voir brièvement les outils nécessaires au développement et comment débuter un projet Unity3D en groupe avec un exemple. Il est possible d'adapter cette inroduction à d'autres développement.

## Boîte à outils

Je vais partir du principe qu'on a pas un rond en poche, mais qu'on a tout de même un ordinateur (avec les spécifications requises pour faire tourner le moteur Unity3D en 2018.217f1) et une connexion internet.

* Visual Studio Code
* GiMP
* Blender
* cmder (Utilisatrice·ur·s windows)
* git-scm (Utilisatrice·ur·s windows)

**Visual Studio Code** (et non pas Community ou Enterprise) est un éditeur de texte Open Source développé par Microsoft. On pourrait tout autant utiliser Notedpad.exe, de base sur windows, ou Notepad++, logiciel libre et souverain.

**GiMP,** suffisant pour faire des textures et des ajustements d'assets 2D.

**Blender**, meilleur logiciel de modélisation 3D qui est gratuit et open source. Utile… pour faire des assets 3D, mais pas que. Exemple, je suis nul en 3D, par contre je peux modéliser un objet et faire un rendu de cet objet pour l'intégrer en 2D dans mon interface graphique plus tard. [https://www.blender.org/](https://www.blender.org/)

**cmder**, c'est optionnel. C'est surtout un joli terminal transparent sur Windows dont il est simple de modifier la police d'écriture. Aussi la version complète a git intégré au terminal. [http://cmder.net/](http://cmder.net/)

**git-scm**, si après avoir installé Visual Studio Code et cmder sur votre ordinateur et que vous n'avez toujours pas de client git, allez vous procurer celui-ci.

Tous les logiciels cités pour la toolchain sont open source et gratuit. Maintenant, on passe dans le monde propriétaire avec Unity3D. En travaillant en groupe je conseille à l'équipe d'avoir la même version téléchargée avec UnityHub (un client qui télécharge les différentes version) On va se baser sur la 2018.217f1 en téléchargeant l'éditeur (il est possible d'ajouter le support d'android pour exporter sur mobile par exemple, mais dans ce cas il faudra télécharger le SDK de Google).

## Serveur Git

En groupe, pour du code, c'est plus pratique d'utiliser un logiciel de gestion de versions décentralisé. Le plus répandu est git, développé par Linus Torvalds à qui l'on doit le noyau linux.

À quoi sert git concrètement ? Cela sert à archiver des fichiers et de garder une trace des modifications apportées. Git s'occupe de copier localement un dossier depuis un dépôt distant; en soit, localement est créé un dépôt. Il est possible d'apporter des modifications et de les appliquer sur le dépôt distant. Cela permet de récupérer du code, de le modifier, de l'archiver et de le partager pour dire les choses plus simplement.

Nous allons utiliser gitlab comme fournisseur de dépôts, il y a des alternatives tel que github, bitbucket ou même en hébergeant soit même son dépôt.

### Clé RSA & Configuration de git

Nous allons avoir besoin d'une clé RSA pour avoir accès à des dépôts gitlab. La clé publique sera téléversée sur cette page: [https://gitlab.com/profile/keys](https://gitlab.com/profile/keys) (cette même page explique comment créer cette clé).

En ouvrant un terminal avec bash (cmder, ou celui de visual studio code) tapez `ssh-keygen` (si vous avez OpenSSH d'installé avec cmder et/ou git-scm cela devrait fonctionner, en cas d'erreur tentez de passer WSL).

Assurez-vous que le chemin pour enregistrer cette clé soit bien `C:\Users\VotreUsername/.ssh/id_rsa` ou alors `~/.ssh/id_rsa` (il s'agit du champ par défaut). Associez ou non cette clé avec un mot de passe (ce mot de passe sera demandé à chaque utilisation de la clé, si vous perdez ce mot de passe il faudra à nouveau générer une clé et l'enregistrer sur gitlab).

D'ailleurs pour enregistrer cette clé, dans un terminal avec un équivalent bash: `cat ~/.ssh/id_rsa.pub | clip` (avec WSL, la même mais au lieu de `clip` c'est `xclip`, sans xclip ce sera `cat` et vous copiez à la mano avec la souris la clé rsa publique). Cette même clé, vous la collez [ici](https://gitlab.com/profile/keys).

Maintenant il ne reste plus qu'à configurer git. Encore une fois depuis un terminal:

```
git config --global user.name "Ton Nom"
git config --global user.email "ton@email"
```

Là, vous êtes fin prêt pour cloner et coder.

### Le tag des clones

On est prêt, il ne manque plus qu'à fork le projet puis le cloner.

Pour le fork, accédez à la page du projet: https://gitlab.com/wolfheimrick/EvasiveDeer

Depuis un terminal, encore, dans un dossier ᴏù vous stockez vos projets, genre pour moi, ça ressemble à ça depuis cmder:

```
C:\Users\wolf
λ pwd
C:\Users\wolf

C:\Users\wolf
λ d:

D:\
λ cd Projects\

D:\Projects
```

et on tape:

```
git clone git@gitlab.com:wolfheimrick/EvasiveDeer.git
```

Si tout se passe bien, le clonage devrait s'effectuer et il est possible maintenant de commencer avec Unity3D.

## Developper Creed : Unity

Après avoir ouvert le projet depuis le HUB (Depuis le hub c'est `open` et ensuite il faut sélectionner le dossier cloné). On va configurer Unity si ce n'est pas déjà fait.

Dans Edit / Preferences/ External Tools par exemple on peut vérifier qu'en "External Script Editor" on a bien `Visual Studio Code`, si ce n'est pas le cas, il est possible de l'associer avec `browse`.

Ensuite dans Project Settings / Editor, on vérifie qu'on a bien an Mode `Visible Meta Files` de sélectionné ainsi que `Force Text` dans respectivement "Version Control" et "Asset Serialization".

Maintenant que tout est prêt, on peut observer la scène qui est ouverte par défaut et se familiariser avec l'interface.

Avant de terminer on peut, peut-être faire notre premier script ensemble, l'ajouter au dépôt, et le soumettre (le but étant d'utiliser git, je ne vais pas expliquer le code dans cette partie).

En cliquant sur `EmptyGameObject` depuis **Hierarchy**, on obtient des informations supplémentaires dans **Inspector**. Il nous est possible d'ajouter un **Component**, plusieurs choix nous est proposé mais ce qui nous intéresse c'est **New Script**. Entrons un nom, genre `FirstScript`.

Celui-ci est créé dans vos Assets. Mettez-le dans le dossier script si vous tenez à garder une bonne organisation. En double cliquant sur ce fichier depuis assets, il devrait s'ouvrir depuis Visual Studio Code et vous devriez voir ceci:

```C#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
```

Virez tout et remplacez le code par:

```C#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstScript : MonoBehaviour {

	private const string TEXT = "hello world";
	private TextMesh textMesh;

	void Start () {
		textMesh = gameObject.AddComponent<TextMesh>();
		textMesh.text = TEXT;
	}
}
```

Ce code une fois enregistré est prêt à être executé: en pressant le bouton triangulaire de lecture de feu les magnétoscopes on devrait obtenir notre premier "Hello World".

Pour ajouter ce code, le commit et le push, la procédure est la suivante.

1. `git add ./<path>/FirstScript.cs`
2. `git commit -m "un message intelligent pour dire ce qu'on a fait"`
3. `git pull` (pull sert à obtenir les dernières mises à jour du dépôt)
4. `git push`

Et voilà.